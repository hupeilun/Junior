#include <stdio.h>  
#include <unistd.h>  
#include <sys/stat.h>  
#include <sys/types.h>  
  
#include <stdlib.h>  
#include <time.h>  
  
int main(int argc, char **argv){  
  
  struct stat st;  
  
  if(argc != 2){  
    fprintf(stderr, "Usage: %s <file_pathname> \n", argv[0]);  
        exit(EXIT_FAILURE);  
    }  
  
    if(stat(argv[1], &st) == -1){  
        perror("stat");  
        exit(EXIT_SUCCESS);  
    }  
  
    printf("文件类型：                ");  
    switch(st.st_mode & S_IFMT){  
      case S_IFBLK:  printf("block device\n");            break;  
      case S_IFCHR:  printf("character device\n");        break;  
      case S_IFDIR:  printf("directory\n");               break;  
      case S_IFIFO:  printf("FIFO/pipe\n");               break;  
      case S_IFLNK:  printf("symlink\n");                 break;  
      case S_IFREG:  printf("普通文件\n");            break;  
      case S_IFSOCK: printf("socket\n");                  break;  
      default:       printf("unknown?\n");                break;  
  }  
    
  printf("I-node :            %ld\n", (long) st.st_ino);    
  printf("硬链接:               %ld\n", (long) st.st_nlink);  
  printf("权限:                UID=%ld   GID=%ld\n", (long) st.st_uid, (long) st.st_gid);  
  printf("device containing file id:%ld \n", (long) st.st_dev);  
  printf("device id:                %ld \n", (long) st.st_rdev);  
  printf("文件大小:                %lld bytes\n", (long long) st.st_size);  
  printf("Preferred I/O block size: %ld bytes\n", (long) st.st_blksize);  
  printf("Blocks allocated:         %lld\n", (long long) st.st_blocks);  
  printf("最近访问：    %s", ctime(&st.st_ctime));  
  printf("最近更改:         %s", ctime(&st.st_atime));  
  printf("最近改动:   %s", ctime(&st.st_mtime));  
  
  exit(EXIT_SUCCESS);  
}
