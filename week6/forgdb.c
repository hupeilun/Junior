#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int  gi=0;
int main()
{
	int li=0;
	static int si=0;
	int i=0;

	pid_t pid = fork();
	if(pid == -1){
		exit(-1);
	}
	else if(pid == 0){
		for(i=0; i<5; i++){
			printf("child li:%d\n", li++);
			sleep(1);
			printf("child gi:%d\n", gi++);
			printf("child si:%d\n", si++);
		}
		exit(0);
		
	}
	else{
		for(i=0; i<5; i++){
			printf("parent li:%d\n", li++);
			printf("parent gi:%d\n", gi++);
			sleep(1);
			printf("parent si:%d\n", si++);
		}
	exit(0);	
	
	}
	return 0;
}
