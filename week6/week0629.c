#include <stdio.h>
#include <stdbool.h>
 
union {
    int number;
    char s;
} test;
 
bool testBigEndin() {
    test.number = 0x12345678;
    return (test.s == 0x12);
}
 
int main(int argc, char **argv) {
    if (testBigEndin()) {
        printf("5329的笔记本电脑是大端\n");
    } else {
        printf("5329的笔记本电脑是小端\n");
    }
} 
