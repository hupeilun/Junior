#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include<sys/types.h>
#include<sys/stat.h>
#include<fcntl.h>
#include<unistd.h>

void tc(int i,int j, char ch[]);
void tx(int i,int j, char ch[]);
void run(int fd,int flag[]);


int main(int argc,char *argv[])
{
    int i;
    int flag[2];
    int fd;
    fd=open(argv[argc-1],O_RDONLY,0);

    if(fd==-1){
        printf("ERROR!\n");
        exit(0);
    }

    for(i=1;i<argc-1;i++){
    if(strcmp(argv[i], "-tc")==0){
        flag[i-1] = 1;
    }
    if(strcmp(argv[i], "-tx1")==0){
        flag[i-1] = 1;
    }
    }
    //FILE *file=fopen(argv[argc-1],"r");
    run(fd,flag);
    close(fd);

    return 0;
}

void run(int fd,int flag[]){
    char ch[18];
    int i=0,j=0;
    if(flag[0] + flag[1] == 2) {
        while(read(fd,&ch,17)!=NULL){
        tc(i,j,ch);
        j++;
        tx(i,j,ch);
        printf("\n");
        }
        j++;
        printf("%07o",16*(j-1)+i);
        printf("\n");
    }
    else if(flag[0] == 1) {
        while(read(fd,&ch,17)!=NULL){
        tc(i,j,ch);
        j++;
        //tx(i,j,ch);
        printf("\n");
        }
        j++;
        printf("%07o",16*(j-1)+i);
        printf("\n");
    }
    else if(flag[1] == 1){
        while(read(fd,&ch,17)!=NULL){
        tx(i,j,ch);
        j++;
        printf("\n");
        }
        j++;
        printf("%07o",16*(j-1)+i);
        printf("\n");
    }
    //fclose(file);
}

void tc(int i,int j, char ch[])
{
    printf("%07o",16*j);
    j++;

    for(i=0;i<16;i++)
    {
        if(ch[i]=='\n')
        {   i++;
            putchar(' ');
            printf("\\n");

        }
        if(ch[i]=='\0')
            break;
        putchar(' ');
        putchar(' ');
        printf("%c", ch[i]);
        putchar(' ');
    }
    printf("\n");
}

void tx(int i,int j, char ch[])
{
    printf("       ");
    for(i=0;i<16;i++)
        {
            if(ch[i]=='\n')
            {   i++;
                printf("%3x ",'\n');
            }
            if(ch[i]=='\0')
            break;
            printf("%3x ",ch[i]);
        }
}
