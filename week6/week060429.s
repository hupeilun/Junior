g:
	pushl	%ebp
	movl	%esp, %ebp
	movl	8(%ebp), %eax
	addl	$3, %eax
	popl	%ebp
	ret
f:
	pushl	%ebp
	movl	%esp, %ebp
	subl	$16, %esp
	movl	$30, -4(%ebp)
	pushl	8(%ebp)
	call	g
	addl	$4, %esp
	movl	%eax, %edx
	movl	-4(%ebp), %eax
	addl	%edx, %eax
	leave
	ret
main:
	pushl	%ebp
	movl	%esp, %ebp
	pushl	$8
	call	f
	addl	$4, %esp
	addl	$1, %eax
	leave
	ret
