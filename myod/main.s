	.file	"main.c"
	.section	.rodata
.LC0:
	.string	"od -tx -tc "
.LC1:
	.string	"%s"
.LC2:
	.string	"r"
.LC3:
	.string	" %c   "
	.text
	.globl	main
	.type	main, @function
main:
.LFB2:
	.cfi_startproc
	leal	4(%esp), %ecx
	.cfi_def_cfa 1, 0
	andl	$-16, %esp
	pushl	-4(%ecx)
	pushl	%ebp
	.cfi_escape 0x10,0x5,0x2,0x75,0
	movl	%esp, %ebp
	pushl	%ecx
	.cfi_escape 0xf,0x3,0x75,0x7c,0x6
	subl	$68, %esp
	movl	%gs:20, %eax
	movl	%eax, -12(%ebp)
	xorl	%eax, %eax
	subl	$12, %esp
	pushl	$.LC0
	call	printf
	addl	$16, %esp
	subl	$8, %esp
	leal	-62(%ebp), %eax
	pushl	%eax
	pushl	$.LC1
	call	__isoc99_scanf
	addl	$16, %esp
	subl	$8, %esp
	pushl	$.LC2
	leal	-62(%ebp), %eax
	pushl	%eax
	call	fopen
	addl	$16, %esp
	movl	%eax, -68(%ebp)
	subl	$12, %esp
	pushl	-68(%ebp)
	call	fgetc
	addl	$16, %esp
	movb	%al, -69(%ebp)
	jmp	.L2
.L5:
	cmpb	$10, -69(%ebp)
	jne	.L3
	subl	$12, %esp
	pushl	$10
	call	putchar
	addl	$16, %esp
	jmp	.L4
.L3:
	movsbl	-69(%ebp), %eax
	subl	$8, %esp
	pushl	%eax
	pushl	$.LC3
	call	printf
	addl	$16, %esp
.L4:
	subl	$12, %esp
	pushl	-68(%ebp)
	call	fgetc
	addl	$16, %esp
	movb	%al, -69(%ebp)
.L2:
	cmpb	$-1, -69(%ebp)
	jne	.L5
	subl	$12, %esp
	leal	-62(%ebp), %eax
	pushl	%eax
	call	tc
	addl	$16, %esp
	subl	$12, %esp
	pushl	$10
	call	putchar
	addl	$16, %esp
	subl	$12, %esp
	leal	-62(%ebp), %eax
	pushl	%eax
	call	tx
	addl	$16, %esp
	nop
	movl	-12(%ebp), %eax
	xorl	%gs:20, %eax
	je	.L6
	call	__stack_chk_fail
.L6:
	movl	-4(%ebp), %ecx
	.cfi_def_cfa 1, 0
	leave
	.cfi_restore 5
	leal	-4(%ecx), %esp
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE2:
	.size	main, .-main
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
