	.file	"tx.c"
	.section	.rodata
.LC0:
	.string	"r"
.LC1:
	.string	" %x  "
	.text
	.globl	tx
	.type	tx, @function
tx:
.LFB2:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$24, %esp
	subl	$8, %esp
	pushl	$.LC0
	pushl	8(%ebp)
	call	fopen
	addl	$16, %esp
	movl	%eax, -12(%ebp)
	subl	$12, %esp
	pushl	-12(%ebp)
	call	fgetc
	addl	$16, %esp
	movb	%al, -13(%ebp)
	jmp	.L2
.L5:
	cmpb	$10, -13(%ebp)
	jne	.L3
	subl	$12, %esp
	pushl	$10
	call	putchar
	addl	$16, %esp
	jmp	.L4
.L3:
	movsbl	-13(%ebp), %eax
	subl	$8, %esp
	pushl	%eax
	pushl	$.LC1
	call	printf
	addl	$16, %esp
.L4:
	subl	$12, %esp
	pushl	-12(%ebp)
	call	fgetc
	addl	$16, %esp
	movb	%al, -13(%ebp)
.L2:
	cmpb	$-1, -13(%ebp)
	jne	.L5
	subl	$12, %esp
	pushl	-12(%ebp)
	call	fclose
	addl	$16, %esp
	nop
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE2:
	.size	tx, .-tx
	.ident	"GCC: (Ubuntu 5.4.0-6ubuntu1~16.04.4) 5.4.0 20160609"
	.section	.note.GNU-stack,"",@progbits
